/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ITOMandel;

import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import static ITOMandel.MandelbrotThreaded.mand;

/**
 *
 * @author Vairis
 */
public class MandelbrotColumn implements Callable<List<Color>>{
    double yc;
    int max;
    int n;
    double size;
    double x0;

    public MandelbrotColumn(double yc, int max, int n, double size, double x0) {
        this.yc = yc;
        this.max = max;
        this.n = n;
        this.size = size;
        this.x0 = x0;
    }


    
    @Override
    public List<Color> call() {
        List<Color> column=new ArrayList<>();
        for (int j = 0; j < n; j++) {
                double y0 = yc - size/2 + size*j/n;
                Complex z0 = new Complex(x0, y0);
                int gray = max - mand(z0, max);
                Color color = new Color(gray, gray, gray);
                column.add(color);
            }
        return column;
    }
    
    
    
}
