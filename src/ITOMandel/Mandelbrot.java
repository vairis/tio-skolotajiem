
/******************************************************************************
 *  Compilation:  javac Mandelbrot.java
 *  Execution:    java Mandelbrot xc yc size
 *  Dependencies: StdDraw.java
 *
 *  Plots the size-by-size region of the Mandelbrot set, centered on (xc, yc)
 *
 *  % java Mandelbrot -0.5 0 2
 * https://introcs.cs.princeton.edu/java/32class/Mandelbrot.java

 ******************************************************************************/
package ITOMandel;

import java.awt.Color;
import java.time.Duration;
import java.time.Instant;
public class Mandelbrot {
   // return number of iterations to check if c = a + ib is in Mandelbrot set
    public static void main(String[] args)  {
        double xc   = 0;//Double.parseDouble(args[0]);
        double yc   = 0;//Double.parseDouble(args[1]);
        double size = 2;//Double.parseDouble(args[2]);

        int n   = 1024;   // create n-by-n image
        int max = 255;   // maximum number of iterations

        Picture picture = new Picture(n, n);
        Instant start = Instant.now();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                double x0 = xc - size/2 + size*i/n;
                double y0 = yc - size/2 + size*j/n;
                Complex z0 = new Complex(x0, y0);
                int gray = max - mand(z0, max);
                Color color = new Color(gray, gray, gray);
                picture.set(i, n-1-j, color);
            }
        }
        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        System.out.println("Time elapsed:"+timeElapsed);
        picture.show();
    }
    public static int mand(Complex z0, int max) {
        Complex z = z0;
        for (int t = 0; t < max; t++) {
            if (z.abs() > 2.0) return t;
            z = z.times(z).plus(z0);
        }
        return max;
    }
}
