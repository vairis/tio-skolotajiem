
/******************************************************************************
 *  Compilation:  javac Mandelbrot.java
 *  Execution:    java Mandelbrot xc yc size
 *  Dependencies: StdDraw.java
 *
 *  Plots the size-by-size region of the Mandelbrot set, centered on (xc, yc)
 *
 *  % java Mandelbrot -0.5 0 2
 * https://introcs.cs.princeton.edu/java/32class/Mandelbrot.java

 ******************************************************************************/
package ITOMandel;

import java.awt.Color;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
public class MandelbrotThreaded {
   // return number of iterations to check if c = a + ib is in Mandelbrot set
    public static void main(String[] args) throws ExecutionException, InterruptedException  {
        double xc   = 0;//Double.parseDouble(args[0]);
        double yc   = 0;//Double.parseDouble(args[1]);
        double size = 2;//Double.parseDouble(args[2]);

        int n   = 1024;   // create n-by-n image
        int max = 255;   // maximum number of iterations

        Picture picture = new Picture(n, n);
        Instant start = Instant.now();
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        List<Future<List<Color>>> futures= new ArrayList<>();
        
        for (int i = 0; i < n; i++) {
            double x0 = xc - size/2 + size*i/n;
            Future<List<Color>> futureColumn= executorService.submit(new MandelbrotColumn(yc, max, n, size, x0));
            futures.add(futureColumn);
        }
        executorService.shutdown();
        for (int i = 0; i < n; i++) {
            List<Color> thisColumn=futures.get(i).get();
            for (int j = 0; j < n; j++) {
                picture.set(i, n-1-j, thisColumn.get(j));
        
            }
        }
            
        Instant finish = Instant.now();
        long timeElapsed = Duration.between(start, finish).toMillis();
        System.out.println("Time elapsed:"+timeElapsed);
        picture.show();
    }
    public static int mand(Complex z0, int max) {
        Complex z = z0;
        for (int t = 0; t < max; t++) {
            if (z.abs() > 2.0) return t;
            z = z.times(z).plus(z0);
        }
        return max;
    }
}
